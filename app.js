require('dotenv').config();

const express = require("express"),
  session = require("express-session"),
  app = express(),
  bodyParser = require("body-parser"),
  mongoose = require("mongoose"),
  passport = require("passport"),
  LocalStrategy = require("passport-local"),
  methodOverride = require("method-override"),
  flash = require("connect-flash"),
  Campground = require("./models/campground"),
  Comment = require("./models/comment"),
  User = require("./models/user"),
  seedDB = require("./seed"),
  mongoStore = require('connect-mongo')(session);

// Requiring routes
const commentRoutes = require("./routes/comments"),
  reviewRoutes = require("./routes/reviews"),
  campgroundRoutes = require("./routes/campgrounds"),
  indexRoutes = require("./routes/index");

mongoose.connect(
    process.env.MONGODB, {
      useNewUrlParser: true
    }
  )
  .then(() => {
    console.log("Connected to MongoDB");
  })
  .catch(err => {
    console.log("Connection to MongoDB failed!", err);
  });
mongoose.set("useFindAndModify", false);
mongoose.set("useCreateIndex", true);

app.use(bodyParser.urlencoded({
  extended: true
}));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(methodOverride("_method"));
app.use(flash());
app.locals.moment = require("moment");
// ==== PASSPORT CONFIG ======//
app.use(
  session({
    secret: process.env.AppSecret,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 3600000
    },
    store: new mongoStore({
      mongooseConnection: mongoose.connection
    })
  })
);


app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());


// seedDB();

app.use(async function (req, res, next) {
  // Makes sure all the routes hav access
  res.locals.currentUser = req.user; // helps to always keep track of the currently logged-in user.
  if (req.user) {
    try {
      let user = await User.findById(req.user._id).populate("notifications", null, {
        isRead: false
      }).exec();
      res.locals.notifications = user.notifications.reverse();
    } catch (err) {
      console.log(err.message);
    }
  }
  res.locals.error = req.flash("error"); // enables error flash message in all routes
  res.locals.success = req.flash("success"); // enables success flash message in all routes
  next();
});

app.use("/", indexRoutes);
app.use("/campgrounds", campgroundRoutes);
app.use("/campgrounds/:id/comments", commentRoutes);
app.use("/campgrounds/:id/reviews", reviewRoutes);

app.get("*", function (req, res) {
  res.send("This page does not exist");
});

app.listen(process.env.PORT, process.env.IP, function () {
  console.log("The YelpCamp Server Has Started");
});