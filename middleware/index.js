const Campground = require("../models/campground"),
  Comment = require("../models/comment"),
  Review = require("../models/review");

let middlewareObj = {};

middlewareObj.campgroundOwnership = function (req, res, next) {
  var backUrl = req.header("Referer") || "/";
  // console.log("backUrl: " + req.session.returnTo);
  if (req.isAuthenticated()) {
    Campground.findById(req.params.id, function (err, foundCampground) {
      if (err || !foundCampground) {
        req.flash("error", "Campground not found");
        res.redirect(backUrl);
      } else {
        // does user own the campground
        if (foundCampground.author.id.equals(req.user._id) || req.user.isAdmin) {
          next();
        } else {
          req.flash("error", "You do not have permission to do that");
          res.redirect(backUrl);
        }
      }
    });
  } else {
    req.session.returnTo = backUrl;
    req.flash("error", "You need to be logged in to do that");
    res.redirect("/login");
  }
};

middlewareObj.commentOwnership = function (req, res, next) {
  var backUrl = req.header("Referer") || "/";
  if (req.isAuthenticated()) {
    Comment.findById(req.params.comment_id, function (err, foundComment) {
      if (err || !foundComment) {
        req.flash("error", "Comment does not exist");
        res.redirect(backUrl);
      } else {
        if (foundComment.author.id.equals(req.user._id) || req.user.isAdmin) {
          next();
        } else {
          req.flash("error", "You do not have permission to do that");
          res.redirect(backUrl);
        }
      }
    });
  } else {
    req.session.returnTo = backUrl;
    req.flash("You need to be logged in to do that");
    res.redirect("/login");
  }
};

middlewareObj.isLoggedIn = function (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  req.flash("error", "You need to be Logged in to do that");
  req.session.returnTo = req.header("Referer");
  res.redirect("/login");
};

middlewareObj.getUrl = function (req, res, next) {
  var url = req.header("Referer");
  if (!url.includes("login")) {
    req.session.returnTo = req.header("Referer");
  }
  next();
};

middlewareObj.checkReviewOwnership = async function (req, res, next) {
  try {
    if (req.isAuthenticated()) {
      let foundReview = await Review.findById(req.params.review_id);
      if (!foundReview) {
        res.redirect("back");
      } else {
        if (foundReview.author.id.equals(req.user._id) || req.user.isAdmin) {
          next();
        } else {
          throw new TypeError("You don't have permission to do that")
        }
      }
    } else {
      throw new TypeError("You need to be logged in to do that!");
    }
  } catch (err) {
    req.flash("error", err.message);
    return res.redirect("back");
  }
};

middlewareObj.checkReviewExistance = async function (req, res, next) {
  try {
    if (req.isAuthenticated()) {
      let foundCampground = await Campground.findById(req.params.id).populate("reviews").exec();
      if (!foundCampground) {
        throw new TypeError("Campground not found");
      } else {
        // check if req.user._id exists in foundCampground.reviews
        let foundUserReview = foundCampground.reviews.some(function (review) {
          return review.author.id.equals(req.user._id);
        });
        if (foundUserReview) {
          req.flash("error", "You have already written a review for " + foundCampground.name);
          return res.redirect("/campgrounds/" + foundCampground._id);
        }
        next();
      }
    } else {
      throw new TypeError("You need to be logged in to do that!");
    }
  } catch (err) {
    req.flash("error", err.message);
    res.redirect("back");
  }
};

module.exports = middlewareObj;