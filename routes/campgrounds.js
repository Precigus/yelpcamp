const express = require("express"),
  router = express.Router(),
  Campground = require("../models/campground"),
  User = require("../models/user"),
  Comment = require("../models/comment"),
  middleware = require("../middleware"), //index.js is required by default, unless otherwise specified
  Notification = require("../models/notification"),
  Review = require("../models/review"),
  NodeGeocoder = require("node-geocoder"),
  multer = require("multer");

var options = {
  provider: "google",
  httpAdapter: "https",
  apiKey: process.env.GEOCODER_API_KEY,
  formatter: null
};

var geocoder = NodeGeocoder(options);

var storage = multer.diskStorage({
  filename: function(req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }
});
var imageFilter = function(req, file, cb) {
  // accept image files only
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif)$/i)) {
    return cb(new Error("Only image files are allowed!"), false);
  }
  cb(null, true);
};
var upload = multer({
  storage: storage,
  fileFilter: imageFilter
});

var cloudinary = require("cloudinary");
cloudinary.config({
  cloud_name: "hexiumwdb",
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET
});

//INDEX - Show all campgrounds
router.get("/", function(req, res) {
  req.session.returnTo = req.header("Referer");
  var noMatch = false;
  if (req.query.search) {
    const regex = new RegExp(escapeRegex(req.query.search), "gi");
    // Get all campgrounds from DB
    Campground.find(
      {
        name: regex
      },
      function(err, searchResults) {
        if (err) {
          console.log(err);
          req.flash("error", err.message);
        } else {
          if (searchResults.length < 1) {
            noMatch = true;
          }
          res.render("campgrounds/index", {
            campgrounds: searchResults,
            noMatch: noMatch,
            page: "campgrounds"
          });
        }
      }
    );
  } else {
    // Get all campgrounds from DB
    Campground.find({}, function(err, allcampgrounds) {
      if (err) {
        console.log(err);
      } else {
        res.render("campgrounds/index", {
          campgrounds: allcampgrounds,
          noMatch: noMatch,
          page: "campgrounds"
        });
      }
    });
  }
});

//CREATE - Create a campground and add it to the DB
router.post("/", middleware.isLoggedIn, upload.single("image"), function(
  req,
  res
) {
  cloudinary.v2.uploader.upload(req.file.path, function(err, result) {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    }

    geocoder.geocode(req.body.location, async function(err, data) {
      if (err || !data.length) {
        req.flash("error", "Invalid address");
        return res.redirect("back");
      }
      req.body.campground.lat = data[0].latitude;
      req.body.campground.lng = data[0].longitude;
      req.body.campground.location = data[0].formattedAddress;

      // add cloudinary url for the image to the campground object under image property
      req.body.campground.image = {
        url: result.secure_url,
        imageId: result.public_id
      };

      // add author to campground
      req.body.campground.author = {
        id: req.user._id,
        username: req.user.username
      };

      //Create a new campground and save to the DB
      try {
        let campground = await Campground.create(req.body.campground);
        let user = await User.findById(req.user._id)
          .populate("followers")
          .exec();
        let newNotification = {
          username: req.user.username,
          campgroundId: campground.id
        };
        for (const follower of user.followers) {
          let notification = await Notification.create(newNotification);
          follower.notifications.push(notification);
          follower.save();
        }
        res.redirect("/campgrounds/" + campground.id);
      } catch (err) {
        req.flash("error", err.message);
        res.redirect("back");
      }
    });
  });
});

//NEW - Show form to create a new campground
router.get("/new", middleware.isLoggedIn, function(req, res) {
  res.render("campgrounds/new");
});

//SHOW - Display information about a single campground
router.get("/:id", async function(req, res) {
  try {
    let foundCampground = await Campground.findById(req.params.id)
      .populate("comments")
      .populate({
        path: "reviews",
        options: {
          sort: {
            createdAt: -1
          }
        }
      })
      .exec();
    if (!foundCampground) {
      throw new TypeError("Campground not found!");
    } else {
      res.render("campgrounds/show", {
        campground: foundCampground
      });
    }
  } catch (err) {
    req.flash("error", err.message);
    res.redirect("back");
  }
});

// EDIT campground ROUTE
router.get("/:id/edit", middleware.campgroundOwnership, async function(
  req,
  res
) {
  req.session.returnTo = req.header("Referer");
  try {
    let foundCampground = await Campground.findById(req.params.id);
    res.render("campgrounds/edit", {
      campground: foundCampground
    });
  } catch (err) {
    req.flash("error", err.message);
    return res.redirect("back");
  }
});

// UPDATE campground ROUTE
router.put(
  "/:id",
  middleware.campgroundOwnership,
  upload.single("image"),
  async function(req, res) {
    try {
      await delete req.body.campground.rating;
      let foundCampground = await Campground.findById(req.params.id);
      if (req.file) {
        // Delete the file from Cloudinary
        await cloudinary.v2.uploader.destroy(foundCampground.image.imageId);
        // Upload a new image into Cloudinary
        let result = await cloudinary.v2.uploader.upload(req.file.path);
        foundCampground.image = {
          url: result.secure_url,
          imageId: result.public_id
        };
      }
      let data = await geocoder.geocode(req.body.campground.location);
      foundCampground.lng = data[0].longitude;
      foundCampground.lat = data[0].latitude;
      foundCampground.location = data[0].formattedAddress;
      foundCampground.name = req.body.campground.name;
      foundCampground.price = req.body.campground.price;
      foundCampground.description = req.body.campground.description;
      foundCampground.save();
      req.flash("success", "Campground successfully updated!");
      res.redirect("/campgrounds/" + req.params.id);
    } catch (err) {
      req.flash("error", err.message);
      return res.redirect("back");
    }
  }
);

// DELETE campground ROUTE
router.delete("/:id", middleware.campgroundOwnership, async function(req, res) {
  try {
    let campground = await Campground.findById(req.params.id);
    await cloudinary.v2.uploader.destroy(campground.image.imageId);
    await Comment.remove({
      _id: {
        $in: campground.comments
      }
    });
    await Review.remove({
      _id: {
        $in: campground.reviews
      }
    });
    await campground.remove();
    req.flash("success", "Campground deleted successfully");
    res.redirect("/campgrounds");
  } catch (err) {
    if (err) {
      req.flash("error", err.message);
      return res.redirect("/campgrounds");
    }
  }
});
// router.delete("/:id", middleware.campgroundOwnership, function (req, res) {
//   Campground.findById(req.params.id, async function (err, campground) {
//     if (err) {
//       req.flash("error", err.message);
//       return res.redirect("back");
//     }
//     try {
//       await cloudinary.v2.uploader.destroy(campground.image.imageId);
//       campground.remove();
//       req.flash("success", "Campground deleted successfully");
//       res.redirect("/campgrounds");
//     } catch (err) {
//       if (err) {
//         req.flash("error", err.message);
//         return res.redirect("back");
//       }
//     }
//   });
// });

function escapeRegex(text) {
  return text.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&");
}

module.exports = router;
