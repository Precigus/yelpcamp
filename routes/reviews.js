const express = require("express"),
  router = express.Router({
    mergeParams: true
  }),
  Campground = require("../models/campground"),
  Review = require("../models/review"),
  middleware = require("../middleware");


// Reviews INDEX
router.get("/", async function (req, res) {
  try {

    let campground = await Campground.findById(req.params.id).populate({
      path: "reviews",
      options: {
        sort: {
          createdAt: -1
        }
      } //sorting to show the latest reviews first
    }).exec();
    res.render("reviews/index", {
      campground: campground
    });

  } catch (err) {

    req.flash("error", err.message);
    return res.redirect("back");

  }
});

// Reviews NEW
router.get("/new", middleware.isLoggedIn, middleware.checkReviewExistance, async function (req, res) {
  try {

    let campground = await Campground.findById(req.params.id);
    res.render("reviews/new", {
      campground: campground
    });

  } catch (err) {

    req.flash("error", err.message);
    return res.redirect("back");

  }
});

// Reviews CREATE
router.post("/", middleware.isLoggedIn, middleware.checkReviewExistance, async function (req, res) {
  try {

    let campground = await Campground.findById(req.params.id).populate("reviews").exec();
    let review = await Review.create(req.body.review);
    review.author = {
      id: req.user._id,
      username: req.user.username
    }
    review.campground = campground;
    review.save();
    campground.reviews.push(review);
    campground.rating = calculateAverage(campground.reviews);
    campground.save();
    req.flash("success", "Your review has been successfully added!");
    res.redirect("/campgrounds/" + campground._id);

  } catch (err) {

    req.flash("error", err.message);
    return res.redirect("back");
  }
});

// Reviews EDIT
router.get("/:review_id/edit", middleware.checkReviewOwnership, async function (req, res) {
  try {

    let foundReview = await Review.findById(req.params.review_id);
    res.render("reviews/edit", {
      campground_id: req.params.id,
      review: foundReview
    });

  } catch (err) {

    req.flash("error", err.message);
    return res.redirect("back");

  }
});

//Review UPDATE
router.put("/:review_id", middleware.checkReviewOwnership, async function (req, res) {
  try {

    let updatedReview = await Review.findByIdAndUpdate(req.params.review_id, req.body.review, {
      new: true
    });
    let campground = await Campground.findById(req.params.id).populate("reviews").exec();
    campground.rating = calculateAverage(campground.reviews);
    await campground.save();
    req.flash("success", "Your review has been successfully updated");
    res.redirect("/campgrounds/" + campground._id + "/reviews");

  } catch (err) {
    req.flash("error", err.message);
    return res.redirect("back");
  }
});

// Reviews DELETE
router.delete("/:review_id", middleware.checkReviewOwnership, async function (req, res) {
  try {
    await Review.findByIdAndRemove(req.params.review_id);
    let campground = await Campground.findByIdAndUpdate(req.params.id, {
      $pull: {
        reviews: req.params.review_id
      }
    }, {
      new: true
    }).populate("reviews").exec();
    campground.rating = await calculateAverage(campground.reviews);
    await campground.save();
    req.flash("success", "Your review has been successfully deleted!");
    res.redirect("/campgrounds/" + req.params.id);
  } catch (err) {
    req.flash("error", err.message);
    return res.redirect("back");
  }
});

function calculateAverage(reviews) {
  if (reviews.length === 0) {
    return 0;
  }
  let sum = 0;
  reviews.forEach(review => {
    sum += review.rating;
  });
  return sum / reviews.length;
}

module.exports = router;