var mongoose = require("mongoose");
var Campground = require("./models/campground");
var Comment = require("./models/comment");

var seeds = [{
        name: "Cloud's Rest",
        price: 12,
        lat: 37.865,
        lng: -119.538,
        location: "Yosemite National Park, California, USA",
        author: {
            id: "5c2979e2865ed63474217c20",
            username: "Test"
        },
        image: {
            "url": "https://res.cloudinary.com/hexiumwdb/image/upload/v1547089171/awymc7mwfqzkk6fahvcf.jpg",
            "imageId": "awymc7mwfqzkk6fahvcf"
        },
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
    },
    {
        name: "Desert Oasis",
        price: 12,
        lat: 37.865,
        lng: -119.538,
        location: "Yosemite National Park, California, USA",
        author: {
            id: "5c2979e2865ed63474217c20",
            username: "Test"
        },
        image: {
            "url": "https://res.cloudinary.com/hexiumwdb/image/upload/v1547089171/awymc7mwfqzkk6fahvcf.jpg",
            "imageId": "awymc7mwfqzkk6fahvcf"
        },
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
    },
    {
        name: "Messa Black",
        price: 12,
        lat: 37.865,
        lng: -119.538,
        location: "Yosemite National Park, California, USA",
        author: {
            id: "5c2979e2865ed63474217c20",
            username: "Test"
        },
        image: {
            "url": "https://res.cloudinary.com/hexiumwdb/image/upload/v1547089171/awymc7mwfqzkk6fahvcf.jpg",
            "imageId": "awymc7mwfqzkk6fahvcf"
        },
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum"
    }
]

async function seedDB() {
    try {
        await Comment.deleteMany({});
        console.log("campgrounds removed");
        await Campground.deleteMany({});
        console.log("comments removed");

        for (const seed of seeds) {
            let campground = await Campground.create(seed);
            console.log("Campground created");
            let comment = await Comment.create({
                text: "This place is great, but I wish there was internet",
                author: {
                    id: "5c2979e2865ed63474217c20",
                    username: "Test"
                }
            });
            console.log("comment created");
            campground.comments.push(comment);
            campground.save();
            console.log("comment added to campground");
        }
    } catch (err) {
        console.log(err);
    }
}

module.exports = seedDB;